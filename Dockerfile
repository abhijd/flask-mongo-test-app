FROM python:2.7.12

COPY . /yousi/


WORKDIR /yousi

EXPOSE 5000


RUN pip install -r requirements.txt 


CMD ["python", "/yousi/yousi/yousi.py"]