A Test Flask-Mongo API
=================
A test API with the python micro framework called flask and MongoDB. This is done while in the process of learning flask.


[![pipeline status](https://gitlab.com/abhijd/flask-mongo-test-app/badges/master/pipeline.svg)](https://gitlab.com/abhijd/flask-mongo-test-app/commits/master)


Platforms 
------------
* Ubuntu xenial 

Requirements
------------

* Python 2.7+ 


### Running Locally

* Make sure you have all the requirements above already installed
* Clone the repo in any local directory 
* Run terminal from that directory
* [optional]  install, create and activate a  Virtual env  
```bash
sudo pip install virtualenv
virtualenv venv
source venv/bin/activate
```

* In terminal, enter the following code to install all dependency
```bash
pip install -r requirements.txt
```

* start a Mongodb server with the data structures illustrated in  section "MongoDB Data Structure"

* create a file to store local Mongo credentials : 
```bash
touch yousi/configs/local.py
```

* Add the Mongo Configs in local.py in the following manner: ( Replace <*********> with actual content)
```python
yousician_password = "<*********>"
yousician_username = "<*********>"
yousician_auth_source = "<*********>"
mongo_endpoint = "<*********>"
```

* Run the App:
```bash
python yousi/yousi.py
```

_VOILA! You have done it. The App is running, (Hopefully)_

#  API Tests:

For automatic testing of the API routes, from the project directory run the following:
```bash
python yousi_tests.py

```



#  List of routes implemented:
- `GET/POST /songs`
  - Returns a list of songs with some details on them
  - Possible to paginate songs.

The GET request returns all the songs from the collection as a stream of data.

For pagination I have used used POST /songs with two parameters ‘page’ and ‘perPage’.
‘page’ corresponds to the batch number of a potential stream of data.
‘perPage’ is the amount of songs that needs to be returned for each page.

### Example:
``` bash
curl -X 'POST' -H "Content-Type: application/json" --data '{"page":"1","perPage":"15"}' 127.0.0.1:5000/songs
```
In the above request, 'page' = 1   and 'perPage' = 15
So the songs from index 0 to 14 will be returned in response
```bash
curl -X 'POST' -H "Content-Type: application/json" --data '{"page":"1","perPage":"15"}' 127.0.0.1:5000/songs
```
Next, in the above request 'page' = 2   and 'perPage' = 15
So, the songs from index 15 to 29 will be returned in response.

In this way, the App, calling the API, can ask for any particular batch and amount as long as the index is not out of bound. Validation for index upper bound is also there in the code 


- `GET /songs/avg/difficulty`
  - Takes an optional parameter "level" to select only songs from a specific level.
  - Returns the average difficulty for all songs.
  A precomputed collection has been used. Where the averages for each level and all songs are pre- calculated and stored. This allows significant computation shedding from the program for each request.

A mongo collection called precomputed has been used, where the averages for each level and all songs are pre-calculated  and  stored.  This  allows  significant  computation  shedding  from  the  program  for  each request. 
A function called _update_precomp_avg_ diff has also been added that can be attached to the (fictional) add song function of admin, so that each time a new song is added, the average of only that song’s level will be re calculated and restored in the precomputed collection of Mongo.


- `GET /songs/search`
  - Takes in parameter a 'message' string to search.
  - Return a list of songs. The search should take into account song's artist and title. The search should be case insensitive.

To search for song, MongoDB’s awesome feature of text index based search has been used.

Text Search index has been added to Tittle and Artist so that search account both those fields.

The return query also includes a search score, that corresponds to the quality of match, which is already sorted according to that score so that the application that is calling the API can easily list the search with the most relevant one in the top. 


- `POST /songs/rating`
  - Takes in parameter a "song_id" and a "rating". _**Parameter must be in JSON format**_
  - This call adds a rating to the song. Ratings should be between 1 and 5.
  This adds the rating as required after validation that the rating is between the range.
Every time a new rating is added, parameter in the collection representing number of ratings, lowest rating, highest rating and average rating are all updated for that single song. This allows fast lookup for the next route.  


- `GET /songs/avg/rating/<song_id>`
  - Returns the average, the lowest and the highest rating of the given song id.
  This just fetches that one song corresponding the id and returns the required parameters that has already been added in the above route’s function. 



# MongoDB Data Structure

* Two collections, `songs` and `precomputed`, have been used in this API

The Data Structures of the collections are illustrated below:

 

### Mongo Collection _**songs**_ Data Structure

**field `author` and `title` are indexed together as text index for search feature** 

![picture alt](songs_structure.png "Data Structure of songs")


### Mongo Collection _**precomputed**_ Data Structure


![picture alt](precomputed_structure.png "Data Structure of  precomputed")



## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is free to be used for any learning or educational purpose but cannot be used for commercial use without proper consent of the author.

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required for the project. 