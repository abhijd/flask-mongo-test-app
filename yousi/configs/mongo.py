from pymongo import MongoClient
import os

try:
    if os.environ['gitlab']  ==  'yes':
        mongo_endpoint  =  os.environ['MONGO_ENDPOINT']
        yousician_username  =  os.environ['YOUSICIAN_USERNAME']
        yousician_password  =  os.environ['YOUSICIAN_PASSWORD']
        yousician_auth_source  =  os.environ['YOUSICIAN_AUTH_SOURCE']
    else:
        from local import mongo_endpoint, yousician_username, yousician_password, yousician_auth_source    
except:
    from local import mongo_endpoint, yousician_username, yousician_password, yousician_auth_source  



# MONGO Connection Config
client = MongoClient(mongo_endpoint,
                    username=yousician_username,
                    password=yousician_password,
                    authSource=yousician_auth_source
                    )

db = client.yousician   # Mongo Database
song_collection = db.songs  # Mongo Collection Name
precomputed_collection  =  db.precomputed 