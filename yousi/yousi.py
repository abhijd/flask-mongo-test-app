from flask import Flask, request, Response, json, jsonify
from bson.json_util import dumps
from bson.objectid import ObjectId


from configs.mongo import song_collection, precomputed_collection


##########################################################
#   APP CONFIGS                                     #######
##########################################################
app = Flask(__name__)


app.debug = True
# app.host  =  '127.0.0.1'  # default
# app.port  =  '5000'        # default

#### CHANGING DEFAULT MIMETYPE TO JSON FOR ALL RESPONSE  ####
class JsonResponse(Response):
    default_mimetype = 'application/json'
app.response_class = JsonResponse



##########################################################
#   /songs         ENDPOINT                         #######
##########################################################
@app.route('/songs', methods=['POST', 'GET'])
def show_songs():
    # - GET /songs
    #   - Returns a list of songs with some details on them
    #   - Add possibility to paginate songs. [NOT SURE YET WHAT IT IS MEANT IN THIS CONTEXT, SHOULD I SEND EMAIL and ASK??]
    if request.method == 'POST':   # curl -X 'POST' -H "Content-Type: application/json" --data '{"page":"2","perPage":"3"}' 127.0.0.1:5000/songs
        #   - Add possibility to paginate songs. 
        if request.headers['Content-type'] == 'application/json':
            data = request.json
            if 'page' and 'perPage' in data:
                try:
                    page = int (data['page'])
                    per_page = int (data['perPage'])
                except:
                    return json.dumps( {'error': 'page and perPage must be integers'})

                total_songs = song_collection.find().count()
                upper_index = page * per_page

                if  upper_index  > total_songs :
                    return json.dumps( {'error': 'index exceeded'})

                songs =  song_collection.find()[ (page-1) * per_page : page * per_page   ] 
                return dumps(songs)

        return json.dumps( {'error': 'must send in json form'})
        
    else:
        # - Returns a list of songs with some details on them
        return dumps(  song_collection.find( {} , {"_id":0, "released": 0}  )  )   # id and release field are excluded 


#######################################################
#   /songs/avg/difficulty    ENDPOINT            #######
#######################################################

@app.route('/songs/avg/difficulty')
def show_avg_difficulty():
    # - GET /songs/avg/difficulty
    #   - Takes an optional parameter "level" to select only songs from a specific level.
    #   - Returns the average difficulty for all songs.
    if 'level' in request.args:
        try:
            lvl = int(request.args['level'])
            precomputed = precomputed_collection.find_one( {"level":lvl })
            if precomputed == None: 
                return json.dumps({ 'error': 'Sorry Rockstar, No songs in this level'})
            return jsonify( {'average': precomputed['avg'] , 'level': lvl} )
        except ValueError:
            return app.response_class(response=json.dumps({"error": "Sorry Rockstar, Level must be an Interger value"
                                                        }), status=400)
    else:
        precomputed = precomputed_collection.find_one( {"level":0})
        return json.dumps( {'average': precomputed['avg'] , 'level': 'all'} )


# THIS is to update the precomputed collection whenever a new song is added
@app.route('/admin/add-song/<new_song_id>')
def update_precomp_avg_diff(new_song_id):
    new_song = song_collection.find_one({"_id": ObjectId(new_song_id) })

    precomputed =  precomputed_collection.find( { "$or": [ { "level" : 0 }, { "level": int(new_song['level']) } ]  } )  

    if precomputed.count() == 1:
        precomputed_collection.insert({'level': int(new_song['level'] ), 'type' : 'avg_diff' , 'num_songs': 0 , 'avg': 0.000 })
        precomputed =  precomputed_collection.find( { "$or": [ { "level" : 0 }, { "level": int(new_song['level']) } ]  } )  

    for doc in precomputed:
        total=  ( float( doc["avg"] ) * float( doc["num_songs"] ) ) + float( new_song["difficulty"] )
        num_songs = (int(doc["num_songs"]) + 1)
        new_avg = total / (int(doc["num_songs"]) + 1)      
        precomputed_collection.find_one_and_update({"_id": doc['_id']},{"$set":{"avg":new_avg, "num_songs": num_songs  }})

    return jsonify({ 'Added': new_song_id})


# Function that computes the avearge every time a request is made, 
# NOTE not much merciful to computing power I guess, so moving on to alternative method  
@app.route('/songs/avg/difficulty0')
def show_avg_difficulty0():
    # - GET /songs/avg/difficulty0
    #   - Takes an optional parameter "level" to select only songs from a specific level.
    #   - Returns the average difficulty for all songs.
    if 'level' in request.args:
        #  Below,lets try to catch some nasty error together. Only integer is accepted  
        try:
            lvl = int(request.args['level'])  
            songs = song_collection.find({"level": lvl})
            return send_avg_diff_resp(songs, lvl)
        except ValueError:
            return app.response_class(response=json.dumps({"error":
                                                            {
                                                                "message": "send me integer level silly",
                                                                "code": 400
                                                            }
                                                        }), status=400)
    else:
        songs = song_collection.find()
        return send_avg_diff_resp(songs, 'all')

def send_avg_diff_resp(songs_cursor, level):
    count = 0
    difficulty = 0
    for song in songs_cursor:
        difficulty += song["difficulty"]
        count += 1
    return json.dumps({"average_difficulty":
        {
            "level": level,
            "value": difficulty / count,
            "total_songs": count
        }
    })



#######################################################
#   /songs/search   ENDPOINT                     #######
#######################################################

@app.route('/songs/search')
def search_song():
    # - GET /songs/search
    #   - Takes in parameter a 'message' string to search.
    #   - Return a list of songs. The search should take into account song's artist and title. 
    #      The search should be case insensitive.

    if 'message' in request.args:
        songs = song_collection.find( 
                                {  '$text' : { '$search' : request.args['message'] } }, 
                                { 'score': { '$meta': "textScore" } } 
                            )
        songs.sort([('score', {'$meta': 'textScore'})])
                            
        return dumps(songs)
    else:
        return json.dumps({'error': 'No search string received :('})


#######################################################
#   /songs/rating   ENDPOINT                     #######
#######################################################
# curl -X 'POST' -H "Content-Type: application/json" --data '{"song_id":"5a8836d927ef1c02ef90961a","rating":"4"}' 127.0.0.1:5000/songs/rating
# curl -X 'POST'  --data '{"song_id":"5a8836d927ef1c02ef90961a","rating":"4"}' 127.0.0.1:5000/songs/rating
@app.route('/songs/rating', methods=['POST'])
def add_rating():
    # - POST /songs/rating
    #   - Takes in parameter a "song_id" and a "rating"
    #   - This call adds a rating to the song. Ratings should be between 1 and 5.
    if request.headers['Content-type'] == 'application/json':
        data = request.json

        if 'song_id' and 'rating' in data:
            
            # Rating validation and error handling
            try:
                rating = float( data['rating'] )
            except:
                return json.dumps({'error': 'Ohho Rockstar, rating must be a number'})
            if rating <1 or rating >5:
                return json.dumps({'error': 'Ohho Rockstar, rating must be between 1 nad 5'})

            # Song ID validation and error handling
            is_valid = validate_songID(data['song_id'])
            if not is_valid[0]:
                return is_valid[1]
            
            song = is_valid[1]
            id = ObjectId(data['song_id'])
            
            # If this is reached, both rating and song_id are valid
            # Awesome, lets actually add the rating now

            if 'rating' and 'rCount' not in song:
                song_collection.find_one_and_update({"_id": id } ,{"$set":{"rating":rating, "rCount": 1, 'hRating': rating , 'lRating' : rating}})
            else:
                rating_old = song['rating']
                rCount_old = song['rCount']

                rating_new = ( (  rating_old * float(rCount_old) ) + rating ) / ( rCount_old+ 1)

                if (rating  > song['hRating']):
                    song_collection.find_one_and_update({"_id": id } ,{"$set":{"rating":rating_new, "rCount": rCount_old+ 1 , 'hRating':rating }})
                elif (rating  < song['lRating']):
                    song_collection.find_one_and_update({"_id": id } ,{"$set":{"rating":rating_new, "rCount": rCount_old+ 1 , 'lRating':rating }})
                else:
                    song_collection.find_one_and_update({"_id": id } ,{"$set":{"rating":rating_new, "rCount": rCount_old+ 1 }})
            return json.dumps({'message': 'thanks Rockstar for the rating, your feedback matters to us :)',
                                'song_id': data['song_id']
                            })
        
    return json.dumps({'error': 'Ohho Rockstar, please send the parameters song_id and rating as json'})


#######################################################
#   /songs/avg/rating/<song_id>   ENDPOINT       #######
#######################################################
# curl 127.0.0.1:5000/songs/avg/rating/<song_id>
@app.route('/songs/avg/rating/<song_id>')
def show_avg_rating(song_id):
    # - GET /songs/avg/rating/<song_id>
    #   - Returns the average, the lowest and the highest rating of the given song id.
    
    # Song ID validation and error handling
    is_valid = validate_songID(song_id)
    if not is_valid[0]:
        return is_valid[1]
    
    song = is_valid[1]

    return json.dumps({'average': song['rating'] , 'highest' : song['hRating'] , 'lowest' : song['lRating']   })


def validate_songID(song_id):
    
    # Song ID validation and error handling
    try:
        id = ObjectId(song_id)
    except:
        message = json.dumps({'error': 'Ohho Rockstar, not a valid id'})
        return [False, message]
    song = song_collection.find_one({"_id": id })
    if song == None:
        message = json.dumps({'error': 'Ohho Rockstar, no song found with that id'})
        return [False, message]
    
    return [True, song]
   

#######################################################
#   RUN, Run RUNNNNNN                            #######
#######################################################

if __name__ == '__main__':
    # app.run(host, port, debug, options)
    app.run()
