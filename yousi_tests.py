from yousi import yousi
import unittest 
from json import loads as json_loads, dumps as json_dumps

######################
#   CONTROL DATA    #
######################
# Oh No! Hard Coded Data for testing.

# Difficulty Averages
difficulty_average = {
                'all': 10.323636363636364,
                '3' : 2,
                '6' : 6,
                '9' : 9.693333333333333,
                '13': 14.096
}



def is_it_json(data):
    try:
        data_dict = json_loads(data)
    except Exception as er:
        return [False, er]
    return [True, data_dict ]



class SongsAPItests(unittest.TestCase): 

    def setUp(self):
        # creates a test client
        self.app = yousi.app.test_client()
        # propagate the exceptions to the test client
        self.app.testing = True

    def tearDown(self):
        pass 


    ######################
    #   /songs  TEST    #
    ######################

    def test_songs(self):
        result = self.app.get('/songs')
        # print jsonify(result)
        # self.assertEquals(result.data, dict(success=True))
        truth  =  is_it_json(result.data)
        if truth[0]:
            data_dict = truth[1] 
            self.assertEqual( len(data_dict) , 11)      
            pass
        else:
            self.fail("Sadly JSON data is not returned :(.\nReturned Data:\n"+ str(result.data))
    

    ####################################
    #   /songs/avg/difficulty  TEST    #
    ####################################

    def test_avg_diff(self):
        # Test with level parameter
        keys = difficulty_average.keys()
        for key in keys:
            if key == 'all':
                result = self.app.get('/songs/avg/difficulty')
            else:
                result = self.app.get('/songs/avg/difficulty?level='+key)

            truth  =  is_it_json(result.data)
            if truth[0]:
                data_dict = truth[1]   
                self.assertAlmostEqual(data_dict['average'], difficulty_average[key], 4, 'Difficulty Average Assertion failed at song  level: '+key)    
            else:
                self.fail("Sadly JSON data is not returned :(.\nReturned Data:\n"+ str(result.data))

    ####################################################
    #   /songs/search                 TEST              #
    ####################################################

    def test_search(self):
        result = self.app.get('/songs/search?message=yousician got power')
        # print jsonify(result)
        # self.assertEquals(result.data, dict(success=True))
        truth  =  is_it_json(result.data)
        if truth[0]:
            data_dict = truth[1]
            self.assertEquals( data_dict[0]['title'] , "You've Got The Power" )    
            pass
        else:
            self.fail("Sadly JSON data is not returned :(.\nReturned Data:\n"+ str(result.data))


    ####################################################
    #   /songs/avg/rating/<song_id>  TEST              #
    ####################################################

    def test_rating_fetch(self):
        # Fetch a test data from db
        test_song = yousi.song_collection.find_one({'rating': { '$exists': 'true' } })

        result = self.app.get('/songs/avg/rating/'+str( test_song['_id'] ) )


        truth  =  is_it_json(result.data)
        if truth[0]:
            data_dict = truth[1]
            self.assertEquals(data_dict['average'], test_song['rating'] )  
            self.assertEquals(data_dict['highest'], test_song['hRating'])
            self.assertEquals(data_dict['lowest'], test_song['lRating'])
            pass
        else:
            self.fail("Sadly JSON data is not returned :(.\nReturned Data:\n"+ str(result.data))

    
    ####################################################
    #   /songs/rating  TEST              #
    ####################################################
    
    def test_rating_add(self):
        
        result = self.app.post( '/songs/rating'  , data =  json_dumps( {"song_id":"5a8836d927ef1c02ef90961a","rating":"5"} ), headers={'Content-type': 'application/json'} ) 

        truth  =  is_it_json(result.data)
        if truth[0]:
            # specific test for result
            data_dict = truth[1] 
            self.assertEquals( data_dict['message'], "thanks Rockstar for the rating, your feedback matters to us :)")      
            pass
        else:
            self.fail("Sadly JSON data is not returned :(.\nReturned Data:\n"+ str(result.data))

 

suite = unittest.TestLoader().loadTestsFromTestCase(SongsAPItests)

test_well = unittest.TextTestRunner(verbosity=2).run(suite)

if test_well.failures != [] or test_well.errors != []:
    raise Exception('Noooo! failed test')
